//khai báo thư viện express
const express = require('express');
const reviewMiddleware = require('../middlewares/reviewMiddleware');

//tạo router
const reviewRouter = express.Router();

//sủ dụng middle ware
reviewRouter.use(reviewMiddleware);

//get all reviews
reviewRouter.get('/reviews', (request, response) => {
    console.log("Get all reviews");
    response.json({
        message:'Get All reviews'
    })
});

//get a review
reviewRouter.get('/reviews/:reviewid', (request, response) => {
    let id = request.params.reviewid;

    console.log(`Get reviewid = ${id}`);
    response.json({
        message:`Get reviewid = ${id}`
    })
})

//create a review
reviewRouter.post('/reviews', (request, response) => {
    let body = request.body;

    console.log('create a review');
    console.log(body);
    response.json({
        ...body
    })
});


//update a review
reviewRouter.put('/reviews/:reviewid', (request, response) => {
    let id = request.params.reviewid;
    let body = request.body;

    console.log('update a review');
    console.log({id, ...body});
    response.json({
        message: {id, ...body}
    })
})

//delete a review
reviewRouter.delete('/reviews/:reviewid', (request, response) => {
    let id = request.params.reviewid;

    console.log('delete a review' + id);
    response.json({
        message: 'delete a review' + id
    })
})

module.exports = { reviewRouter };
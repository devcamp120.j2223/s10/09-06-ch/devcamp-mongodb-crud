//import course model
const { default: mongoose } = require('mongoose');
const courseModel = require('../models/courseModel');

//get all courses
const getAllCourses = (request, response) => {
    //B1. thu thập dữ liệu (bỏ qua)
    //B2. kiểm tra dữ liệu (bỏ qua)
    //B3. thực hiện thao tác dữ liệu
    courseModel.find((error, data) => {
        if (error) {
            response.status(500).json({
                message:`Internal server error: ${error.message}`
            })        
        } else {
            response.status(200).json({
                data
            })
        }
    })
};

//get a course by id
const getCourseById = (request, response) => {
    //B1. thu thập dữ liệu (bỏ qua)
    let id = request.params.courseid;

    //B2. kiểm tra dữ liệu 
    if (!mongoose.Types.ObjectId.isValid(id)) {
        response.status(400).json({
            message:"id is invalid!"
        })
    } else {
        //B3. thực hiện thao tác dữ liệu
        courseModel.findById(id, (error, data) => {
            if (error) {
                response.status(500).json({
                    message:`Internal server error: ${error.message}`
                })        
            } else {
                response.status(200).json({
                    data
                })
            }
        })        
    }
    

};

//create a course
const createCourse = (request, response) => {
    //B1: thu thập dữ liệu
    let body = request.body;

    //B2: kiểm tra dữ liệu
    if (!body.title) {
        response.status(400).json({
            message:"title is required!"
        })
    } else if (!Number.isInteger(body.noStudent) || body.noStudent < 0) {
        response.status(400).json({
            message:"noStudent is invalid!"
        }) 
    } else {
        //B3: thực hiện các thao tác nghiệp vụ
        let course = {
            _id : mongoose.Types.ObjectId(),
            title : body.title,
            description : body.description,
            noStudent  :body.noStudent
        }

        courseModel.create(course, (error, data) => {
            if (error) {
                response.status(500).json({
                    message:`Internal server error: ${error.message}`
                })        
            } else {
                response.status(201).json({
                    data
                })   
            }
        });
    }
};

//update a course by id
const updateCourseById = (request, response) => {
    //B1. Thu thập dữ liệu
    let id = request.params.courseid;
    let body = request.body;

    //B2. Kiểm tra dữ liêu
    if (!mongoose.Types.ObjectId.isValid(id)) {
        response.status(400).json({
            message:"id is invalid!"
        })
    } else if (!body.title) {
        response.status(400).json({
            message:"title is required!"
        })
    } else if (!Number.isInteger(body.noStudent) || body.noStudent < 0) {
        response.status(400).json({
            message:"noStudent is invalid!"
        }) 
    } else {
        //B3: thực hiện các thao tác nghiệp vụ
        let course = {
            title : body.title,
            description : body.description,
            noStudent  :body.noStudent
        }
        courseModel.findByIdAndUpdate(id, course, (error, data) => {
            if (error) {
                response.status(500).json({
                    message:`Internal server error: ${error.message}`
                })        
            } else {
                response.status(200).json({
                    data
                })   
            }
        });
    }
};

//delete a course by id
const deleteCourseById = (request, response) => {
    //B1. Thu thập dữ liệu
    let id = request.params.courseid;

    //B2. Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(id)) {
        response.status(400).json({
            message:"id is invalid!"
        })
    } else {
        //B3: thực hiện các thao tác nghiệp vụ
        courseModel.findByIdAndDelete(id, (error, data) => {
            if (error) {
                response.status(500).json({
                    message:`Internal server error: ${error.message}`
                })        
            } else {
                response.status(204).json({
                    data
                })   
            }
        })
    }
};

//export các hàm ra thành module
module.exports = { getAllCourses, getCourseById, createCourse, updateCourseById, deleteCourseById }